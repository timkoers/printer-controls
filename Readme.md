# Readme
This project is all about monitoring the power consumption of your 3D printer. It is part of a video series that I have made on my YouTube channel.

**Please keep in mind that the code for this project is written around the V2.0 PZEM-004T and uses unreleased ESP-32 UART RX Interrupt.** 
You can find the sources here: [arduino-esp32](https://github.com/timkoers/arduino-esp32/) and [PZEM004T](https://github.com/timkoers/PZEM004T)

![Schematic](https://gitlab.com/timkoers/printer-controls/-/raw/master/Printer-Controls/Printer-Controls.svg)

Feel free to watch them:
Part 1: https://youtu.be/V5WKtJgpAxY
Part 2: https://youtu.be/Is4xUzKoBuQ
Part 3: https://youtu.be/OM0u3rCqGQc

The parts you'll need:
* ESP32 Devkit V1 (36/38 pin): https://s.click.aliexpress.com/e/_ATW9jP
* PZEM004T-V (V2.0/V3.0): https://s.click.aliexpress.com/e/_9HiZ5t
* 230V AC to 12V DC: https://s.click.aliexpress.com/e/_9GzN2L
* 3.3V Buck converter: https://s.click.aliexpress.com/e/_AnnArv
* PCB's: [Gerber files](https://gitlab.com/timkoers/printer-controls/-/blob/master/Printer-Controls/Printer-Controls.zip)
* The 3D Printable enclosure: https://www.thingiverse.com/thing:4729609


