#define CONFIG_ESP32_WIFI_TASK_PINNED_TO_CORE_0 0

#if (CONFIG_ESP32_WIFI_TASK_PINNED_TO_CORE_0)
#warning "Please pin the WiFi task to core 1"`
#endif

#include "Arduino.h"

#define MHz(x) x * 1000000
#define kHz(x) x * 1000

#define OUTPUT_VALUES_Serial 0

#define LOG_TAG "Boot"
#define LOG_TAG_ROTARY_TASK "Rotary-task"
#define LOG_TAG_LCD_TASK "LCD-task"
#define LOG_TAG_DHT_TASK "DHT-task"
#define LOG_TAG_PZEM_TASK "PZEM-task"
#define LOG_TAG_LED_TASK "LED-task"
#define LOG_TAG_WIFI_TASK "WiFi-task"
#define LOG_TAG_RENDER_TASK "Render-task"

#include "sdkconfig.h"
#include <driver/uart.h>

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include "esp32-hal-log.h"
#include "esp_log.h"
#include <assert.h>

//8 seconds WDT
#include <esp_task_wdt.h>

#include <WiFi.h>
#include "HomeEnvironment.h"
#include <SPIFFS.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <PietteTech_DHT.h>
#include <AiEsp32RotaryEncoder.h>

// The PZEM004T used in this project is the V2.0
#include <PZEM004T.h>

#define client_id "Printer-Controls"

#define MQTT_Prefix "Lights/" client_id
#define MQTT_Prefix_Environment "Environment/" client_id
#define MQTT_Prefix_Printer "Switches/" client_id

#define humidity_topic MQTT_Prefix_Environment "/Humidity"
#define temperature_topic MQTT_Prefix_Environment "/Temperature"
#define dewPoint_topic MQTT_Prefix_Environment "/Dewpoint"
#define available_topic client_id "/Available"
#define debug_topic client_id "/Debug"

#define light_power_topic MQTT_Prefix "/Power"
#define light_state_topic MQTT_Prefix "/State"
#define light_brightness_topic MQTT_Prefix "/Brightness"
#define light_brightness_state_topic MQTT_Prefix "/BrightnessState"

#define printer_state_topic MQTT_Prefix_Printer "/State"
#define printer_power_topic MQTT_Prefix_Printer "/Power"

#define printer_voltage_topic client_id "/Printer/Energy/Voltage"
#define printer_current_topic client_id "/Printer/Energy/Current"
#define printer_power_usage_topic client_id "/Printer/Energy/Power"
#define printer_energy_topic client_id "/Printer/Energy/Energy"

#define POWER_PRINTER_START_THRESH 2.0
#define POWER_PRINTER_STOP_THRESH 2.0

/*
connecting Rotary encoder
CLK (A pin) - to any microcontroler intput pin with interrupt -> in this example pin 32
DT (B pin) - to any microcontroler intput pin with interrupt -> in this example pin 21
SW (button pin) - to any microcontroler intput pin -> in this example pin 25
VCC - to microcontroler VCC (then set ROTARY_ENCODER_VCC_PIN -1) or in this example pin 25
GND - to microcontroler GND
*/
#define ROTARY_ENCODER_A_PIN 4
#define ROTARY_ENCODER_B_PIN 2
#define ROTARY_ENCODER_BUTTON_PIN 15
#define ROTARY_ENCODER_VCC_PIN -1 /*put -1 of Rotary encoder Vcc is connected directly to 3,3V; else you can use declared output pin for powering rotary encoder */

#define DHTTYPE DHT22 // Sensor type DHT11/21/22/AM2301/AM2302
#define DHTPIN 19     // This is the CTS pin used for UART2 on the ESP, disable hardware flow control
#define LIGHT_PIN 18  // GPIO2 = D4
#define PRINTER_PIN 5 // GPIO15 = D8

#define LIGHT_DIMMING_POLARITY 1 // 1 for inverted, 0 for non inverted
#define LIGHT_CHANNEL 0
#define LIGHT_PWM_FREQUENCY kHz(10) // Hz
#define LIGHT_RESOLUTION 8          //bits

// Timings
#define WDT_TIMEOUT 8          // sec
#define RENDER_INTERVAL 800    // ms
#define cycleTime 4000         // ms
#define PZEM_TIMEOUT 100       // ms
#define PZEM_LOOP_DELAY 2000   // ms
#define PZEM_UPDATE_DELAY 400  // ms, change this if other tasks are wtd'ing
#define DHT_TIMEOUT 1000       // ms
#define DHT_LOOP_DELAY 2200    // ms
#define LED_LOOP_DELAY 250     // ms
#define ROTARY_LOOP_DELAY 10   // ms
#define MAX_OFF_BRIGHTNESS 150 // 0-255
#define DATA_LENGTH 6

typedef struct DHT_Data
{
  float t, h, d;
} DHT_Data_t;

typedef struct PZEM_Data
{
  float voltage, current, power, energy;
} PZEM_Data_t;

typedef struct PZEM_StartStopData
{
  long timeStart = 0;
  long timeEnd = 0;
  float kwhStart = 0;
  float kwhEnd = 0;
} PZEM_StartStopData_t;

typedef struct MQTT_Publish_Data
{
  char *topic;
  char *data;
  bool retained;
} MQTT_Publish_Data_t;

typedef struct RotaryEncoder_Event
{
  int16_t delta;
  ButtonState buttonState;
} RotaryEncoder_Event_t;

QueueHandle_t pzemQueue;
QueueHandle_t pzemActivationQueue;
QueueHandle_t dhtQueue;
QueueHandle_t displayQueue;
QueueHandle_t mqttPublishQueue;
QueueHandle_t rotaryEncoderQueue;

typedef enum Menu
{
  OffMenu = 0,
  FinishedPowerMenu,
  PowerMenuVA,
  PowerMenuPC,
  EnvironmentMenu,
  WiFiConnectMenu,
  WiFiConnectedMenu,
  WiFiFailedMenu,
  PrinterOffMenu
} Menu_t;

// Forward declarations
void dht_task(void *param);
void wifi_task(void *param);
void pzem_task(void *param);
void lcd_task(void *param);
void led_task(void *param);
void rotary_task(void *param);

void callback(char *incommingTopic, byte *load, unsigned int length);
void render(Menu_t &activeMenu, LiquidCrystal_I2C *lcd);
void connect_to_pzem();
void dht_wrapper(); // must be declared before the lib initialization
void rotary_wrapper();
void rotary_button_wrapper();

AiEsp32RotaryEncoder rotaryEncoder(ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN);
PietteTech_DHT DHT(DHTPIN, DHTTYPE, dht_wrapper);

portMUX_TYPE dhtMux = portMUX_INITIALIZER_UNLOCKED;

TaskHandle_t dhtTask;
TaskHandle_t lcdTask;
TaskHandle_t wifiTask;
TaskHandle_t pzemTask;
TaskHandle_t ledTask;
TaskHandle_t rotaryEncoderTask;

SemaphoreHandle_t xPZEMDataSemaphore;
SemaphoreHandle_t xDHT22DataSemaphore;
SemaphoreHandle_t xMenuSemaphore;
SemaphoreHandle_t xLedSemaphore;
SemaphoreHandle_t xDHTInitializedSemaphore;
SemaphoreHandle_t xPZEMInitializedSemaphore;
SemaphoreHandle_t xActivateSemaphore;

// Globals
bool dhtInitialized = false;
bool pzemrdy = false;
bool activated = false;

bool lightState = false;
int brightness = 0;

long menuInterval;

// This wrapper is in charge of calling
// must be defined like this for the lib work
void IRAM_ATTR dht_wrapper()
{
  portENTER_CRITICAL_ISR(&dhtMux);
  DHT.isrCallback();
  portEXIT_CRITICAL_ISR(&dhtMux);
}

void IRAM_ATTR rotary_wrapper()
{
  rotaryEncoder.readEncoder_ISR();
  if (rotaryEncoderTask != nullptr)
  {
    BaseType_t xHigherPriorityTaskWoken;
    xTaskNotifyFromISR(rotaryEncoderTask, 0x1, eSetBits, &xHigherPriorityTaskWoken);

    if (xHigherPriorityTaskWoken)
    {
      portYIELD_FROM_ISR();
    }
  }
}

void IRAM_ATTR rotary_button_wrapper()
{
  rotaryEncoder.readButton_ISR();
  if (rotaryEncoderTask != nullptr)
  {
    BaseType_t xHigherPriorityTaskWoken;
    xTaskNotifyFromISR(rotaryEncoderTask, 0x1, eSetBits, &xHigherPriorityTaskWoken);

    if (xHigherPriorityTaskWoken)
    {
      portYIELD_FROM_ISR();
    }
  }
}

/*
 * Known bugs:
 * When the printer is turned off (from MQTT), pressing the button again results in the PrinterOffMenu being displayed.
 * After screen timeout, pressing the rotary encoder one more time enables the printer.
 */

/*
 * TODO Features:
 * 3 sec hold to turn off the printer
 */

void setup()
{
  Serial.begin(115200);

  // Turn off the LED
  pinMode(PRINTER_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  digitalWrite(LIGHT_PIN, HIGH);

  dhtQueue = xQueueCreate(6, sizeof(DHT_Data_t));
  pzemQueue = xQueueCreate(8, sizeof(PZEM_Data_t));
  pzemActivationQueue = xQueueCreate(2, sizeof(PZEM_StartStopData_t));
  displayQueue = xQueueCreate(1, sizeof(Menu_t));
  mqttPublishQueue = xQueueCreate(24, sizeof(MQTT_Publish_Data_t));
  rotaryEncoderQueue = xQueueCreate(4, sizeof(RotaryEncoder_Event_t));

  assert(dhtQueue != NULL);
  assert(pzemQueue != NULL);
  assert(pzemActivationQueue != NULL);
  assert(displayQueue != NULL);
  assert(mqttPublishQueue != NULL);
  assert(rotaryEncoderQueue != NULL);

  // Create the semaphores
  vSemaphoreCreateBinary(xPZEMDataSemaphore);
  vSemaphoreCreateBinary(xDHT22DataSemaphore);
  vSemaphoreCreateBinary(xMenuSemaphore);
  vSemaphoreCreateBinary(xLedSemaphore);
  vSemaphoreCreateBinary(xDHTInitializedSemaphore);
  vSemaphoreCreateBinary(xPZEMInitializedSemaphore);
  vSemaphoreCreateBinary(xActivateSemaphore);

  assert(xPZEMDataSemaphore != NULL);
  assert(xDHT22DataSemaphore != NULL);
  assert(xMenuSemaphore != NULL);
  assert(xLedSemaphore != NULL);
  assert(xDHTInitializedSemaphore != NULL);
  assert(xPZEMInitializedSemaphore != NULL);
  assert(xActivateSemaphore != NULL);

  assert(xSemaphoreTake(xPZEMDataSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xDHT22DataSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xMenuSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xLedSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xDHTInitializedSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xPZEMInitializedSemaphore, 10) == pdTRUE);
  assert(xSemaphoreTake(xActivateSemaphore, 10) == pdTRUE);

  assert(xSemaphoreGive(xPZEMDataSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xDHT22DataSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xMenuSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xLedSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xDHTInitializedSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xPZEMInitializedSemaphore) == pdTRUE);
  assert(xSemaphoreGive(xActivateSemaphore) == pdTRUE);

  // Restart the TWDT
  esp_task_wdt_deinit();

  // Set the timeout
  ESP_ERROR_CHECK(esp_task_wdt_init(WDT_TIMEOUT, true)); // Enable panic

  ESP_LOGI(LOG_TAG, "Starting tasks..");

  // Create tasks here
  xTaskCreatePinnedToCore(lcd_task, LOG_TAG_LCD_TASK, 10240, NULL, 4, &lcdTask, 0);
  vTaskDelay(pdMS_TO_TICKS(300));
  xTaskCreatePinnedToCore(led_task, LOG_TAG_LED_TASK, 4096, NULL, 2, &ledTask, 0);
  vTaskDelay(pdMS_TO_TICKS(100));
  xTaskCreatePinnedToCore(rotary_task, LOG_TAG_ROTARY_TASK, 4096, NULL, 5, &rotaryEncoderTask, 0);
  vTaskDelay(pdMS_TO_TICKS(200));
  xTaskCreatePinnedToCore(wifi_task, LOG_TAG_WIFI_TASK, 10240, NULL, 3, &wifiTask, 1);
  vTaskDelay(pdMS_TO_TICKS(300));
  xTaskCreatePinnedToCore(dht_task, LOG_TAG_DHT_TASK, 4096, NULL, 5, &dhtTask, 0);
  vTaskDelay(pdMS_TO_TICKS(400));
  xTaskCreatePinnedToCore(pzem_task, LOG_TAG_PZEM_TASK, 4096, NULL, 5, &pzemTask, 0);
}

void loop()
{
  vTaskDelay(portMAX_DELAY);
}

void dht_task(void *param)
{
  esp_task_wdt_delete(dhtTask);                  // Delete the handle, this one might fail
  ESP_ERROR_CHECK(esp_task_wdt_add(dhtTask));    // Add the handles
  ESP_ERROR_CHECK(esp_task_wdt_status(dhtTask)); // Check the status

  ESP_LOGI(LOG_TAG_DHT_TASK, "Starting DHT task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_DHT_TASK, "Initializing DHT..");

  assert(xSemaphoreTake(xDHTInitializedSemaphore, portMAX_DELAY) == pdTRUE);

  xSemaphoreGive(xDHTInitializedSemaphore);

  ESP_LOGD(LOG_TAG_DHT_TASK, "Initialization of DHT completed!");
  ESP_LOGD(LOG_TAG_DHT_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  for (;;)
  {
    ESP_LOGD(LOG_TAG_DHT_TASK, "DHT Status %d", DHT.getStatus());

    if (!DHT.acquiring() && DHT.getStatus() == DHTLIB_OK)
    {
      if (xSemaphoreTake(xDHTInitializedSemaphore, 0) == pdTRUE)
      {
        dhtInitialized = true;
        xSemaphoreGive(xDHTInitializedSemaphore);
      }

      DHT_Data_t mData;
      mData.t = DHT.getCelsius();
      mData.h = DHT.getHumidity();
      mData.d = DHT.getDewPoint();

      // Reset the acquisition state
      DHT.reset();
      DHT.acquire();

      MQTT_Publish_Data_t mTemperatureData;
      MQTT_Publish_Data_t mHumidityData;
      MQTT_Publish_Data_t mDewPointData;

      mTemperatureData.topic = (char *)temperature_topic;
      mHumidityData.topic = (char *)humidity_topic;
      mDewPointData.topic = (char *)dewPoint_topic;

      // Create all the buffers
      char temperatureBuffer[DATA_LENGTH];
      char humidityBuffer[DATA_LENGTH];
      char dewPointBuffer[DATA_LENGTH];

      // Clear the buffers first
      memset(temperatureBuffer, '\0', DATA_LENGTH);
      memset(humidityBuffer, '\0', DATA_LENGTH);
      memset(dewPointBuffer, '\0', DATA_LENGTH);

      // Fill all the buffers with data
      snprintf(temperatureBuffer, DATA_LENGTH, "%.1f", mData.t);
      snprintf(humidityBuffer, DATA_LENGTH, "%.1f", mData.h);
      snprintf(dewPointBuffer, DATA_LENGTH, "%.1f", mData.d);

      mTemperatureData.data = temperatureBuffer;
      mHumidityData.data = humidityBuffer;
      mDewPointData.data = dewPointBuffer;

      mTemperatureData.retained = true;
      mHumidityData.retained = true;
      mDewPointData.retained = true;

      xQueueSend(mqttPublishQueue, &mTemperatureData, 100);
      xQueueSend(mqttPublishQueue, &mHumidityData, 100);
      xQueueSend(mqttPublishQueue, &mDewPointData, 100);

#if OUTPUT_VALUES_Serial
      ESP_LOGI(LOG_TAG_DHT_TASK, "Humidity (%): %f", mData.h);
      ESP_LOGI(LOG_TAG_DHT_TASK, "Temperature (oC): %f", mData.t);
      ESP_LOGI(LOG_TAG_DHT_TASK, "Dew Point (oC): %f", mData.d);
#endif

      xQueueReset(dhtQueue);

      if (xQueueSend(dhtQueue, &mData, 100) != pdTRUE)
        ESP_LOGW(LOG_TAG_DHT_TASK, "Failed to send DHT data onto the queue");
    }
    else
    {
      // Reset the acquisition state
      DHT.reset();
      DHT.acquire();
    }

    vTaskDelay(pdMS_TO_TICKS(DHT_LOOP_DELAY));
    ESP_ERROR_CHECK(esp_task_wdt_reset()); // This should not fail
  }
}

void lcd_task(void *param)
{
  // Restart the TWDT
  esp_task_wdt_delete(lcdTask);                  // Delete the handle, this one might fail
  ESP_ERROR_CHECK(esp_task_wdt_add(lcdTask));    // Add the handle
  ESP_ERROR_CHECK(esp_task_wdt_status(lcdTask)); // Check the handle

  ESP_LOGI(LOG_TAG_LCD_TASK, "Starting LCD task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_LCD_TASK, "Initializing LCD..");

  LiquidCrystal_I2C lcd(PCF8574_ADDR_A21_A11_A01); // set the LCD address to 0x27

  ESP_LOGD(LOG_TAG_LCD_TASK, "Starting LCD driver..");

  if (!lcd.begin(16, 2)) // 16x2 LCD, this calls Wire.begin() registering the interrupts on the core that this task was started on
    ESP_LOGE(LOG_TAG_LCD_TASK, "Failed to start LiquidCrystal_I2C");

  ESP_LOGD(LOG_TAG_LCD_TASK, "Started LCD driver..");

  lcd.clear();
  lcd.backlight();
  lcd.noAutoscroll();
  lcd.setCursor(0, 0);
  lcd.print("Hi");

  ESP_LOGD(LOG_TAG_LCD_TASK, "LCD initialization complete");

  Menu_t activeMenu = WiFiConnectMenu;

  ESP_LOGD(LOG_TAG_LCD_TASK, "Initialization of LCD completed!");
  ESP_LOGD(LOG_TAG_LCD_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  for (;;)
  {
    render(activeMenu, &lcd);
    ESP_ERROR_CHECK(esp_task_wdt_reset()); // This should not fail
    // Wait for any force render notification, or else wait for the render interval
    if (xTaskNotifyWait(pdFALSE, 0x1, NULL, pdMS_TO_TICKS(RENDER_INTERVAL)) == pdPASS)
      // Force a cycle
      menuInterval = 0;

    ESP_LOGD(LOG_TAG_LCD_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));
  }
}

void led_task(void *param)
{
  esp_task_wdt_delete(ledTask);                  // Delete the handle, this one might fail
  ESP_ERROR_CHECK(esp_task_wdt_add(ledTask));    // Add the handles
  ESP_ERROR_CHECK(esp_task_wdt_status(ledTask)); // Add the handles

  ESP_LOGI(LOG_TAG_LED_TASK, "Starting led task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_LED_TASK, "Initializing LED task..");

  double retFreq = 0.0;

  uint8_t prevBrightness = 0;

  // Don't forget to setup the actual ledc peripheral
  if ((retFreq = ledcSetup(LIGHT_CHANNEL, LIGHT_PWM_FREQUENCY, LIGHT_RESOLUTION), retFreq) <= 0.0)
    ESP_LOGE(LOG_TAG_LED_TASK, "Failed to setup led controller peripheral (%f)", retFreq);

  ledcAttachPin(LIGHT_PIN, LIGHT_CHANNEL);

  ESP_LOGD(LOG_TAG_LED_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  for (;;)
  {
    ESP_LOGD(LOG_TAG_LED_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));
    if (xSemaphoreTake(xLedSemaphore, 0) == pdTRUE)
    {
      if (xSemaphoreTake(xActivateSemaphore, 0) == pdTRUE)
      {
        // Adjust the max brightness to the max current amount
        if (!activated && brightness >= MAX_OFF_BRIGHTNESS)
          brightness = MAX_OFF_BRIGHTNESS;

        xSemaphoreGive(xActivateSemaphore);
      }

      ledcWrite(LIGHT_CHANNEL,
#if LIGHT_DIMMING_POLARITY
                255 - brightness
#else
                brightness
#endif
      );

      if (brightness != prevBrightness)
      {
        prevBrightness = brightness;
        char brightnessBuffer[4];
        snprintf(brightnessBuffer, 4, "%d", brightness);

        MQTT_Publish_Data_t mData;
        mData.topic = (char *)light_brightness_state_topic;
        mData.data = brightnessBuffer;
        mData.retained = true;
        xQueueSend(mqttPublishQueue, &mData, 100);
      }
      xSemaphoreGive(xLedSemaphore);
    }
    vTaskDelay(pdMS_TO_TICKS(LED_LOOP_DELAY));
    ESP_ERROR_CHECK(esp_task_wdt_reset()); // This should not fail
  }
}

void wifi_task(void *param)
{
  esp_task_wdt_delete(wifiTask); // Delete the handle, this one might fail

  ESP_LOGI(LOG_TAG_WIFI_TASK, "Starting wifi task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_WIFI_TASK, "Initializing HomeEnvironment..");

  WiFi.persistent(true);

  HomeEnvironment environment;

  environment.setDebug(false);
  environment.setHeartbeat(false);
  environment.setOnConnectionCallback([](HomeEnvironment *environment) {
    ESP_LOGD(LOG_TAG, "Subscribing to MQTT topics");

    if (!environment->setCallback(callback))
      ESP_LOGE(LOG_TAG, "Failed to set MQTT receive callback");

    // Subscribe here
    environment->subscribe(light_power_topic);
    environment->subscribe(light_brightness_topic);
    environment->subscribe(printer_power_topic);

    ESP_LOGI(LOG_TAG_WIFI_TASK, "Connected to WiFi");
    Menu_t tempMenu = WiFiConnectedMenu;
    if (xQueueSend(displayQueue, &tempMenu, 100) != pdTRUE)
      ESP_LOGE(LOG_TAG_WIFI_TASK, "Failed to publish connected menu item");
  });

  if (environment.initialize())
  {
    ESP_LOGI(LOG_TAG_WIFI_TASK, "Initialized!");
    WiFi.setTxPower(WIFI_POWER_19_5dBm);

    // This should only be called when successful init.
    HomeEnvironment *environmentPointer = &environment;

    WiFi.onEvent([environmentPointer](WiFiEvent_t event, WiFiEventInfo_t info) {
      ESP_LOGI(LOG_TAG_WIFI_TASK, "Calling onWiFiEvent");
      (*(HomeEnvironment *)environmentPointer).onWiFiEvent(event, info);
    });
  }
  else
  {
    ESP_LOGE(LOG_TAG_WIFI_TASK, "Failed to initialize the environment!");
  }

  ESP_LOGD(LOG_TAG_WIFI_TASK, "Initialization of HomeEnvironment completed!");
  ESP_LOGD(LOG_TAG_WIFI_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  for (;;)
  {
    // This needs to be receive, since this is the only task to access this queue
    // Could this be a memory leak, that structs won't get deleted properly?
    MQTT_Publish_Data_t mPublishData;
    while (uxQueueMessagesWaiting(mqttPublishQueue) > 0 && xQueueReceive(mqttPublishQueue, &mPublishData, 0) == pdTRUE && environment.publish(mPublishData.topic, mPublishData.data, mPublishData.retained))
    {
      ESP_ERROR_CHECK_WITHOUT_ABORT(esp_task_wdt_reset());
    };

    environment.loop();
    ESP_ERROR_CHECK_WITHOUT_ABORT(esp_task_wdt_reset());
  }
}

void pzem_task(void *param)
{
  esp_task_wdt_delete(pzemTask);                  // Delete the handle, this one might fail
  ESP_ERROR_CHECK(esp_task_wdt_add(pzemTask));    // Add the handles
  ESP_ERROR_CHECK(esp_task_wdt_status(pzemTask)); // Check the handles

  ESP_LOGI(LOG_TAG_PZEM_TASK, "Starting pzem task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_PZEM_TASK, "Initializing PZEM..");

  // First disable flow control on UART2
  uart_set_hw_flow_ctrl(UART_NUM_2, UART_HW_FLOWCTRL_DISABLE, 0);

  // Power management and all
  HardwareSerial hwserial(2);   // Use hwserial UART2 at pins GPIO (TX) and GPIO3 (RX)
  IPAddress ip(192, 168, 1, 1); // Complete bs that you need to provide an IP, it is serial
  PZEM004T pzem(&hwserial);     // Attach PZEM to hwserial1

  pzem.setUpdateInterval(PZEM_UPDATE_DELAY);

  // Create all the buffers
  char voltageBuffer[DATA_LENGTH];
  char currentBuffer[DATA_LENGTH];
  char powerBuffer[DATA_LENGTH * 2];
  char energyBuffer[DATA_LENGTH * 2];

  ESP_LOGD(LOG_TAG_PZEM_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  for (;;)
  {
    if (xSemaphoreTake(xPZEMInitializedSemaphore, portMAX_DELAY) == pdTRUE)
    {
      if (!pzemrdy)
      {
        ESP_LOGD(LOG_TAG_PZEM_TASK, "Connecting to PZEM...");
        pzemrdy = pzem.setAddress(ip);
      }
      xSemaphoreGive(xPZEMInitializedSemaphore);
      vTaskDelay(pdMS_TO_TICKS(PZEM_LOOP_DELAY));
    }
    else
    {
      ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to acq. semaphore!!");
    }

    if (pzem.update())
    {
      PZEM_Data_t mData;

      bool ok = false;

      // First gather all the data, before actually doing anything
      if (ok = pzem.voltage(mData.voltage), !ok)
        ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to get voltage");

      if (ok = pzem.current(mData.current), !ok)
        ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to get current");

      if (ok = pzem.power(mData.power), !ok)
        ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to get current");

      if (ok = pzem.energy(mData.energy), !ok)
        ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to get energy");

      if (xSemaphoreTake(xPZEMInitializedSemaphore, portMAX_DELAY) == pdTRUE)
      {
        pzemrdy = ok;
        xSemaphoreGive(xPZEMInitializedSemaphore);
      }
      else
      {
        ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to acq. semaphore!!");
      }

      // Clear the buffers first
      memset(voltageBuffer, '\0', DATA_LENGTH);
      memset(currentBuffer, '\0', DATA_LENGTH);
      memset(powerBuffer, '\0', DATA_LENGTH * 2);
      memset(energyBuffer, '\0', DATA_LENGTH * 2);

      // Fill all the buffers with data
      snprintf(voltageBuffer, DATA_LENGTH, "%.1f", mData.voltage);
      snprintf(currentBuffer, DATA_LENGTH, "%.2f", mData.current);
      snprintf(powerBuffer, DATA_LENGTH * 2, "%.2f", mData.power);
      snprintf(energyBuffer, DATA_LENGTH * 2, "%.4f", mData.energy);

      // Send everything via MQTT
      MQTT_Publish_Data_t mVoltageData;
      MQTT_Publish_Data_t mCurrentData;
      MQTT_Publish_Data_t mPowerData;
      MQTT_Publish_Data_t mEnergyData;

      mVoltageData.topic = (char *)printer_voltage_topic;
      mCurrentData.topic = (char *)printer_current_topic;
      mPowerData.topic = (char *)printer_power_usage_topic;
      mEnergyData.topic = (char *)printer_energy_topic;

      mVoltageData.data = voltageBuffer;
      mCurrentData.data = currentBuffer;
      mPowerData.data = powerBuffer;
      mEnergyData.data = energyBuffer;

      mVoltageData.retained = true;
      mCurrentData.retained = true;
      mPowerData.retained = true;
      mEnergyData.retained = true;

      // Send MQTT publishes into the queue
      xQueueSend(mqttPublishQueue, &mVoltageData, 100);
      xQueueSend(mqttPublishQueue, &mCurrentData, 100);
      xQueueSend(mqttPublishQueue, &mPowerData, 100);
      xQueueSend(mqttPublishQueue, &mEnergyData, 100);

      xQueueReset(pzemQueue);

      // Make the power telemetry available system wide
      if (xQueueSend(pzemQueue, &mData, 100) != pdTRUE)
        ESP_LOGW(LOG_TAG_PZEM_TASK, "Failed to put PZEM data on the queue");

#if OUTPUT_VALUES_Serial
      ESP_LOGI(LOG_TAG_PZEM_TASK, "Voltage (V): %s", voltageBuffer);
      ESP_LOGI(LOG_TAG_PZEM_TASK, "Current (A): %s", currentBuffer);
      ESP_LOGI(LOG_TAG_PZEM_TASK, "Power (W): %s", powerBuffer);
      ESP_LOGI(LOG_TAG_PZEM_TASK, "Energy (Wh): %s", energyBuffer);
#endif

      ESP_LOGD(LOG_TAG_PZEM_TASK, "Going to peek into the queue");

      PZEM_StartStopData_t mActivationData;
      // Printer start/end detection
      if (mData.power > POWER_PRINTER_START_THRESH)
      {
        ESP_LOGD(LOG_TAG_PZEM_TASK, "Detected printer start, P is above 2 watts");

        mActivationData.kwhStart = mData.energy;
        mActivationData.timeStart = millis();
      }
      else if (xSemaphoreTake(xActivateSemaphore, 0) == pdTRUE)
      {
        if (activated && xQueuePeek(pzemActivationQueue, &mActivationData, 100) == pdTRUE && mActivationData.timeStart != 0 && mData.power < POWER_PRINTER_STOP_THRESH)
        {
          ESP_LOGD(LOG_TAG_PZEM_TASK, "Detected printer stop, P is less than 2 watts");
          mActivationData.kwhEnd = mData.energy;
          mActivationData.timeEnd = millis();
          Menu_t mMenu = FinishedPowerMenu;
          xQueueSend(displayQueue, &mMenu, portMAX_DELAY);
        }

        xSemaphoreGive(xActivateSemaphore);

        ESP_LOGD(LOG_TAG_PZEM_TASK, "Putting data inside the queue");

        // Delete the objects from the queue here!
        // Data is put into the queue at all times
        if (xQueueReset(pzemActivationQueue) == pdPASS && xQueueSend(pzemActivationQueue, &mActivationData, 100) != pdTRUE)
          ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed putting data inside the queue");

        ESP_LOGD(LOG_TAG_PZEM_TASK, "Data put inside the queue");
      }
    }
    vTaskDelay(pdMS_TO_TICKS(PZEM_LOOP_DELAY));
    ESP_ERROR_CHECK_WITHOUT_ABORT(esp_task_wdt_reset());
  }
}

boolean activate_printer()
{
  ESP_LOGI(LOG_TAG, "Activating printer");

  if (xSemaphoreTake(xActivateSemaphore, portMAX_DELAY) == pdTRUE)
  {
    digitalWrite(PRINTER_PIN, HIGH);
    activated = true;
    xSemaphoreGive(xActivateSemaphore);
    Menu_t mMenu = PowerMenuVA;
    xQueueSend(displayQueue, &mMenu, portMAX_DELAY);

    MQTT_Publish_Data_t mData;
    mData.topic = (char *)printer_state_topic;
    mData.retained = true;
    mData.data = "1";

    ESP_LOGD("Activate Printer", "Turning the printer %s", mData.data);
    ESP_LOGD("Activate Printer", "Putting state on the queue");
    if (xQueueSend(mqttPublishQueue, &mData, 100) != pdTRUE)
      ESP_LOGE("Activate Printer", "Failed to put state on queue");
    return true;
  }

  return false;
}

boolean deactivate_printer()
{
  ESP_LOGI(LOG_TAG, "Deactivating printer");

  if (xSemaphoreTake(xActivateSemaphore, portMAX_DELAY) == pdTRUE)
  {
    digitalWrite(PRINTER_PIN, LOW);
    activated = false;
    xSemaphoreGive(xActivateSemaphore);

    Menu_t mMenu;
    PZEM_StartStopData_t mActivationData;
    if (xQueuePeek(pzemActivationQueue, &mActivationData, 100) == pdTRUE)
    {
      mMenu = FinishedPowerMenu;
    }
    else
    {
      mMenu = OffMenu;
    }

    xQueueSend(displayQueue, &mMenu, portMAX_DELAY);

    MQTT_Publish_Data_t mData;
    mData.topic = (char *)printer_state_topic;
    mData.retained = true;
    mData.data = "0";

    ESP_LOGD("Activate Printer", "Turning the printer %s", mData.data);
    ESP_LOGD("Activate Printer", "Putting state on the queue");
    if (xQueueSend(mqttPublishQueue, &mData, 100) != pdTRUE)
      ESP_LOGE("Activate Printer", "Failed to put state on queue");

    return true;
  }

  return false;
}

void callback(char *incommingTopic, byte *load, unsigned int length)
{
  ESP_LOGD("MQTT_Callback", "MQTT message Callback");
  if (!strcmp(incommingTopic, debug_topic) | !strcmp(incommingTopic, printer_state_topic) | !strcmp(incommingTopic, light_state_topic) | !strcmp(incommingTopic, light_brightness_state_topic))
  {
    return;
  }

  ESP_LOGI("MQTT Callback", "Topic %s, payload %01x %c", incommingTopic, load[0], load);

  if (!strcmp(incommingTopic, printer_power_topic))
  {
    if (load[0] == '1')
    {
      activate_printer();
    }
    else
    {
      deactivate_printer();
    }
  }
  else if (!strcmp(incommingTopic, light_brightness_topic))
  {
    if (xSemaphoreTake(xLedSemaphore, portMAX_DELAY) == pdTRUE)
    {
      brightness = atoi((char *)load);
      if (brightness > 255)
      {
        brightness = 255;
      }
      if (!lightState)
      {
        brightness = 0;
      }

      ESP_LOGI("MQTT_Callback", "Brightness: %d", brightness);
      xSemaphoreGive(xLedSemaphore);
    }
  }
  else if (!strcmp(incommingTopic, light_power_topic))
  {
    lightState = (bool)load[0];

    MQTT_Publish_Data_t mData;
    mData.topic = (char *)light_state_topic;
    mData.data = (char *)(lightState ? "1" : "0");
    mData.retained = true;
    xQueueSend(mqttPublishQueue, &mData, 100);

    ESP_LOGD("MQTT_Callback", "Turning the lights %s", load[0] ? "on" : "off");
  }
}

void render(Menu_t &activeMenu, LiquidCrystal_I2C *lcd)
{
  ESP_LOGD(LOG_TAG_RENDER_TASK, "Rendering..");

  std::unique_ptr<char[]> firstLine(new char[16]);
  std::unique_ptr<char[]> secondLine(new char[16]);

  memset(firstLine.get(), ' ', 16);
  memset(secondLine.get(), ' ', 16);

  // Clear to remove any random chars
  lcd->clear();

  if ((millis() - menuInterval) > cycleTime)
  {
    // Cycle to the next one
    ESP_LOGD(LOG_TAG_RENDER_TASK, "Menu interval timeout.");
    ESP_LOGD(LOG_TAG_RENDER_TASK, "Cycling menu");

    // Check if there is data in the rotaryQueue
    RotaryEncoder_Event_t mEvent;
    xQueueReceive(rotaryEncoderQueue, &mEvent, 0);

    switch (activeMenu)
    {

    case WiFiConnectedMenu:
    {
      ESP_LOGD(LOG_TAG_RENDER_TASK, "Setting to power menu");
      if (xSemaphoreTake(xActivateSemaphore, 0) == pdTRUE)
      {
        ESP_LOGD(LOG_TAG_RENDER_TASK, "Printer activated: %d", activated);

        activeMenu = activated ? activeMenu : OffMenu;
        xSemaphoreGive(xActivateSemaphore);
      }
      else
      {
        ESP_LOGE(LOG_TAG_RENDER_TASK, "Failed to acquire activate semaphore");
      }
      break;
    }

    case PowerMenuVA:
    {
      ESP_LOGD(LOG_TAG_RENDER_TASK, "Setting to environment menu");
      activeMenu = PowerMenuPC;
      break;
    }

    case PowerMenuPC:
    {
      ESP_LOGD(LOG_TAG_RENDER_TASK, "Setting to environment menu");
      activeMenu = EnvironmentMenu;
      break;
    }

    case EnvironmentMenu:
    {
      ESP_LOGD(LOG_TAG_RENDER_TASK, "Setting to power menu");
      activeMenu = PowerMenuVA;
      break;
    }

    case PrinterOffMenu:
    {
      // Time out
      if (mEvent.buttonState == BUT_PUSHED)
      {
        activate_printer();
      }
      else if (mEvent.buttonState != ButtonState::BUT_DISABLED)
      {
        activeMenu = OffMenu;
      }
      break;
    }

    case FinishedPowerMenu:
    {
      if (mEvent.buttonState == BUT_PUSHED)
      {
        // Turn off the thing
        activeMenu = OffMenu;
      }
      break;
    }

    case OffMenu:
    {
      if (mEvent.buttonState == BUT_PUSHED)
      {
        activeMenu = PrinterOffMenu;
      }

      break;
    }
    }
    menuInterval = millis();
  }

  Menu_t tempMenu;
  if (xQueueReceive(displayQueue, &tempMenu, 0) == pdTRUE)
  {
    activeMenu = tempMenu;
    ESP_LOGD(LOG_TAG_RENDER_TASK, "Received menu item from queue");
  }
  else
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "Received no menu item from queue");
  }
  // Only turn on the backlight if a queued item is received and if the display needs to be off
  if (activeMenu != OffMenu)
  {
    lcd->backlight();
  }
  else
  {
    lcd->noBacklight();
  }

  switch (activeMenu)
  {

  case FinishedPowerMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = FinishedPowerMenu");
    PZEM_StartStopData_t mActivationData;
    if (xQueuePeek(pzemActivationQueue, &mActivationData, 10) == pdTRUE)
    {
      // Power menu after print
      long printTime = mActivationData.timeEnd - mActivationData.timeStart;
      long outputTime = ((printTime / 1000) > 3600) ? printTime / 3600000 : printTime / 60000;
      char timeUnit = ((printTime / 1000) > 3600) ? 'h' : 'm';

      snprintf(firstLine.get(), 16, "Dur. : %3d %c", outputTime, timeUnit);

      float printEnergy = mActivationData.kwhEnd - mActivationData.kwhStart;
      float printEnergyOutput = (printEnergy > 1000.0) ? printEnergy / 1000 : printEnergy;
      const char *energyUnit = (printEnergy > 1000.0) ? "kWh" : "Wh";

      snprintf(secondLine.get(), 16, "Energy: %.1f %s", printEnergyOutput, energyUnit);

      break;
    }
    // Fall through if there is nothing in the queue
  }

  case PowerMenuVA:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = PowerMenu");

    PZEM_Data_t mData;
    PZEM_StartStopData_t mActivationData;

    bool ready = false;

    if (xSemaphoreTake(xPZEMInitializedSemaphore, 0) == pdTRUE)
    {
      ready = pzemrdy;
      xSemaphoreGive(xPZEMInitializedSemaphore);
    }
    else
    {
      ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to acq. semaphore");
    }

    if (ready)
    {
      if (xQueuePeek(pzemQueue, &mData, 10) == pdTRUE && xQueuePeek(pzemActivationQueue, &mActivationData, 10) == pdTRUE)
      {
        snprintf(firstLine.get(), 16, "V: %.1f V", mData.voltage);
        snprintf(secondLine.get(), 16, "A: %.1f A", mData.current);
      }
      else
      {
        snprintf(firstLine.get(), 16, "No data in PZEM");
        snprintf(secondLine.get(), 16, "queue");
      }
    }
    else
    {
      snprintf(firstLine.get(), 16, "PZEM module not");
      snprintf(secondLine.get(), 16, "initialized");
    }
    // Or just don't show anything and fall through
    break;
  }

  case PowerMenuPC:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = PowerMenuPC");

    PZEM_Data_t mData;
    PZEM_StartStopData_t mActivationData;

    bool ready = false;

    if (xSemaphoreTake(xPZEMInitializedSemaphore, 0) == pdTRUE)
    {
      ready = pzemrdy;
      xSemaphoreGive(xPZEMInitializedSemaphore);
    }
    else
    {
      ESP_LOGE(LOG_TAG_PZEM_TASK, "Failed to acq. semaphore");
    }

    if (ready)
    {
      if (xQueuePeek(pzemQueue, &mData, 10) == pdTRUE && xQueuePeek(pzemActivationQueue, &mActivationData, 10) == pdTRUE)
      {
        snprintf(firstLine.get(), 16, "P: %.1f W", mData.power);

        float consumedEnergy = mData.energy - mActivationData.kwhStart;

        const char *consumedEnergyUnit = (consumedEnergy > 1000.0) ? "kWh" : "Wh";
        consumedEnergy = (consumedEnergy > 1000.0) ? consumedEnergy / 1000.0 : consumedEnergy;

        snprintf(secondLine.get(), 16, "C: %.1f %s", consumedEnergy, consumedEnergyUnit);

        if (mData.energy > 9000000)
        {
          memset(firstLine.get(), ' ', 16);
          memset(secondLine.get(), ' ', 16);

          snprintf(firstLine.get(), 16, "Please clear kWh");
          snprintf(secondLine.get(), 16, "reading on PZEM");
        }
      }
      else
      {
        snprintf(firstLine.get(), 16, "No data in PZEM");
        snprintf(secondLine.get(), 16, "queue");
      }
    }
    else
    {
      snprintf(firstLine.get(), 16, "PZEM module not");
      snprintf(secondLine.get(), 16, "initialized");
    }
    // Or just don't show anything and fall through
    break;
  }

  case EnvironmentMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = EnvironmentMenu");
    DHT_Data_t mData;
    if (xSemaphoreTake(xDHTInitializedSemaphore, 0) == pdTRUE && dhtInitialized)
    {
      xSemaphoreGive(xDHTInitializedSemaphore);
      if (xQueuePeek(dhtQueue, &mData, 10) == pdTRUE)
      {
        snprintf(firstLine.get(), 16, "T: %.1f C", mData.t);
        snprintf(secondLine.get(), 16, "H: %.1f %%", mData.h);
      }
      else
      {
        snprintf(firstLine.get(), 16, "No data in DHT");
        snprintf(secondLine.get(), 16, "queue");
      }
    }
    else
    {
      snprintf(firstLine.get(), 16, "DHT module not");
      snprintf(secondLine.get(), 16, "initialized");
    }

    break;
  }

  case WiFiConnectMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = WiFiConnectMenu");
    snprintf(firstLine.get(), 16, "Connecting..");
    break;
  }

  case WiFiConnectedMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = WiFiConnectedMenu");
    snprintf(firstLine.get(), 16, "Connected");
    break;
  }
  case OffMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = OffMenu");
    lcd->noBacklight();
    break;
  }

  case WiFiFailedMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = WiFiFailedMenu");
    snprintf(firstLine.get(), 16, "Failed to");
    snprintf(secondLine.get(), 16, "connect");
    break;
  }

  case PrinterOffMenu:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = PrinterOffMenu");
    snprintf(firstLine.get(), 16, "Deactivated");
    snprintf(secondLine.get(), 16, "Press to turn on");
    break;
  }

  default:
  {
    ESP_LOGD(LOG_TAG_RENDER_TASK, "ActiveMenu = DEFAULT");
    snprintf(firstLine.get(), 16, "DEFAULT");
    snprintf(secondLine.get(), 16, "DEFAULT");
    break;
  }
  }

  ESP_LOGD(LOG_TAG_RENDER_TASK, "Printing chars");

  lcd->clear();
  lcd->home();
  lcd->noAutoscroll();
  lcd->print(firstLine.get());
  lcd->setCursor(0, 1);
  lcd->print(secondLine.get());
  lcd->display();

  ESP_LOGD(LOG_TAG_RENDER_TASK, "Rendered..");
}

void rotary_task(void *param)
{
  esp_task_wdt_delete(rotaryEncoderTask);                  // Delete the handle, this one might fail
  ESP_ERROR_CHECK(esp_task_wdt_add(rotaryEncoderTask));    // Add the handles
  ESP_ERROR_CHECK(esp_task_wdt_status(rotaryEncoderTask)); // Check the status

  ESP_LOGI(LOG_TAG_ROTARY_TASK, "Starting Rotary Encoder task on core %d..", xPortGetCoreID());
  ESP_LOGI(LOG_TAG_ROTARY_TASK, "Initializing encoder..");

  rotaryEncoder.setup(rotary_wrapper, rotary_button_wrapper);
  rotaryEncoder.setBoundaries(0, 20, true);
  rotaryEncoder.enable();

  ESP_LOGD(LOG_TAG_ROTARY_TASK, "Initialization of Rotary Encoder completed!");
  ESP_LOGD(LOG_TAG_ROTARY_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

  RotaryEncoder_Event_t mData;

  bool shouldPublish = false;

  for (;;)
  {
    ESP_LOGD(LOG_TAG_ROTARY_TASK, "Max stack usage: %d", uxTaskGetStackHighWaterMark(NULL));

    mData.delta = rotaryEncoder.encoderChanged();
    mData.buttonState = rotaryEncoder.currentButtonState();

    if (mData.buttonState == ButtonState::BUT_PUSHED)
    {
      shouldPublish = true;
      // Reset the timer thingy
    }
    else
    {
      // Write the button disables state, or the system will think this is also a button press
      mData.buttonState = ButtonState::BUT_DISABLED;
      shouldPublish = false;
    }

    if (!shouldPublish)
      shouldPublish = mData.delta != 0;

    if (shouldPublish)
    {
      xQueueReset(rotaryEncoderQueue);
      xQueueSend(rotaryEncoderQueue, &mData, 100);

      if (lcdTask != nullptr)
      {
        xTaskNotify(lcdTask, 0x1, eSetBits);
      }
    }

    ESP_ERROR_CHECK(esp_task_wdt_reset());
    uint32_t notifyValue;
    xTaskNotifyWait(pdFALSE, ULONG_MAX, &notifyValue, pdMS_TO_TICKS(RENDER_INTERVAL * 4));

    ESP_ERROR_CHECK(esp_task_wdt_reset());
  }
}